/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final_project;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author MinhTuan
 */
public class Board implements ActionListener {

    public static int rows = 3;
    public static int columns = 3;
    public static JFrame jfm;
    public static GridLayout grid;
    public static JButton[][] btn;

    public Board() {
        Board.jfm = new JFrame();
        Board.btn = new JButton[rows][columns];
        Board.grid = new GridLayout();
    }

    public Board(int rows, int columns) {
        Board.rows = rows;
        Board.columns = columns;
        Board.jfm = new JFrame();
        Board.btn = new JButton[rows][columns];
        Board.grid = new GridLayout();
    }

    public void show(int weight, int height) {
        jfm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jfm.setVisible(true);
        jfm.setResizable(false);
        jfm.setBounds(0, 0, weight, height);
        jfm.setLayout(grid);

        grid.setColumns(columns);
        grid.setRows(rows);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                btn[i][j] = new JButton();
                btn[i][j].setBackground(Color.WHITE);
                btn[i][j].setFont(new Font(Font.SERIF, Font.ITALIC, 80));
                jfm.add(btn[i][j]);
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                btn[i][j].addActionListener(this);
            }
        }

        initialization();
    }

    public void initialization() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                btn[i][j].setText("");
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        JButton ml = (JButton) ae.getSource();
        ml.setText(Controller.X_Player);
        Point p = Controller.find_Best_Move();
        btn[p.x][p.y].setText(Controller.O_Player);
        if (Controller.is_Win("X")) {
            System.out.print("Ahiih");
        }
    }

}
