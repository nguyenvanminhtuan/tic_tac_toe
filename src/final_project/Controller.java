/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package final_project;

import java.util.ArrayList;

/**
 *
 * @author MinhTuan
 */
public class Controller {

    public static String NO_Player = "";
    public static String X_Player = "X";
    public static String O_Player = "O";
    public static Point Computer_Player;
    public static Board board;

    public static void start_Game() {
        board = new Board(3, 3);
        board.show(500, 500);
    }

    public static boolean isGame_Over() {
        return is_Win(X_Player) || is_Win(O_Player);
    }

    public static boolean is_Win(String current_Player) {
        if (Board.btn[0][0].getText().equals(Board.btn[1][1].getText()) && Board.btn[0][0].getText().equals(Board.btn[2][2].getText()) && Board.btn[0][0].getText().equals(current_Player) && !"".equals(Board.btn[0][0].getText())) {
            return true;
        }
        if (Board.btn[0][2].getText().equals(Board.btn[1][1].getText()) && Board.btn[0][2].getText().equals(Board.btn[2][0].getText()) && Board.btn[0][2].getText().equals(current_Player) && !"".equals(Board.btn[0][2].getText())) {
            return true;
        }

        for (int i = 0; i < 3; i++) {
            if (Board.btn[i][0].getText().equals(Board.btn[i][1].getText()) && Board.btn[i][0].getText().equals(Board.btn[i][2].getText()) && Board.btn[i][0].getText().equals(current_Player)) {
                if (!"".equals(Board.btn[i][0].getText())) {
                    return true;
                }
            }
            if (Board.btn[0][i].getText().equals(Board.btn[1][i].getText()) && Board.btn[0][i].getText().equals(Board.btn[2][i].getText()) && Board.btn[0][i].getText().equals(current_Player)) {
                if (!"".equals(Board.btn[0][i].getText())) {
                    return true;
                }
            }
        }
        return false;

    }

    public static boolean is_Draw() {
        for (int i = 0; i < Board.rows; i++) {
            for (int j = 0; j < Board.columns; j++) {
                if ("".equals(Board.btn[i][j].getText())) {
                    return false;
                }
            }
        }
        return !(is_Win(X_Player) || is_Win(O_Player));
    }

    public static ArrayList<Point> getAvailablgeCells() {
        ArrayList<Point> list = new ArrayList();

        for (int i = 0; i < Board.rows; i++) {
            for (int j = 0; j < Board.columns; j++) {
                if (Board.btn[i][j].getText().equals(NO_Player)) {
                    list.add(new Point(i, j));
                }
            }
        }

        return list;
    }

    public boolean placeMove(Point p, String current_Player) {
        if (!NO_Player.equals(Board.btn[p.x][p.y].getText())) {
            return false;
        }

        Board.btn[p.x][p.y].setText(current_Player);
        return true;
    }

    public static int minimax(boolean is_X_Player, int dept) {
        if (is_Win(X_Player)) {
            return 10 - dept;
        }
        if (is_Win(O_Player)) {
            return -10 + dept;
        }

        ArrayList<Point> list = getAvailablgeCells();

        if (list.isEmpty()) {
            return 0;
        }
        int bestVal;
        if (is_X_Player) {
            bestVal = Integer.MIN_VALUE;
            for (int i = 0; i < list.size(); i++) {
                Board.btn[list.get(i).x][list.get(i).y].setText(X_Player);
                int value = minimax(false, dept + 1);
                bestVal = Math.max(bestVal, value);
                Board.btn[list.get(i).x][list.get(i).y].setText(NO_Player);
            }
            return bestVal;
        } else if (!is_X_Player) {
            bestVal = Integer.MAX_VALUE;
            for (int i = 0; i < list.size(); i++) {
                Board.btn[list.get(i).x][list.get(i).y].setText(O_Player);
                int value = minimax(true, dept + 1);
                bestVal = Math.min(bestVal, value);
                Board.btn[list.get(i).x][list.get(i).y].setText(NO_Player);
            }
            return bestVal;
        }
        return 0;
    }

    public static Point find_Best_Move() {
        Point p = null;
        int value;
        int bestval = Integer.MAX_VALUE;
        ArrayList<Point> list = getAvailablgeCells();
        for (int i = 0; i < list.size(); i++) {
            Board.btn[list.get(i).x][list.get(i).y].setText(O_Player);
            value = minimax(true, 0);
            if (value < bestval) {
                bestval = Math.min(value, bestval);
                p = list.get(i);
            }
            Board.btn[list.get(i).x][list.get(i).y].setText(NO_Player);

        }
        return p;
    }
}
